# Introduction
This is the Github project of 56i-mtserver-wiki. It uses Tiddlywiki and a Gitlab repo to keep afloat. 

To contribute, add to the wiki from your browser, either using the copy from this repo or the older (±25 minutes) version live on the server.
